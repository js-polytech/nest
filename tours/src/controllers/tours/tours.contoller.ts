import { Controller, Delete, Post } from '@nestjs/common';
import { Get, Param, UseGuards } from '@nestjs/common/decorators';
import { JwtAuthGuard } from 'src/services/authentification/jwt-auth.guard/jwt-auth.guard.service';
import { ToursService } from 'src/services/tours/tours.service';
import { ITour } from 'src/interface/tours';

@Controller('tours')
export class ToursController {
    constructor(private tourService: ToursService) {
    }
    //@UseGuards(JwtAuthGuard)
    @Post()
    initTours(): Promise<ITour[]> {
        this.tourService.generateTours();
        return  this.tourService.getAllTours();
    }

    @UseGuards(JwtAuthGuard)
    @Get()
   getAllTours(): Promise <ITour[]> {
       return this.tourService.getAllTours();
    }
    
    @UseGuards(JwtAuthGuard)
    @Get(":id")
    getTourById(@Param ("id")id): Promise <ITour> {
        return this.tourService.getTourById(id);
    }

    @Delete()
    removeAllTours(): Promise<[]> {
       return  this.tourService.deleteTours();
    }
}


     