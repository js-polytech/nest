import { Module } from '@nestjs/common';
import { ToursController} from './tours.contoller';
import {MongooseModule} from "@nestjs/mongoose";
import {Tour, TourSchema} from "../../schemas/tour";
import { jwtConstants } from 'src/static/private/constants';
import { ToursService } from 'src/services/tours/tours.service';
import { JwtStrategyService } from 'jwt-strategy.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';

@Module({
  controllers: [ToursController],
  imports:  [MongooseModule.forFeature([{ name: Tour.name, schema: TourSchema }]),
  PassportModule,
  JwtModule.register( {
    secret: jwtConstants.secret
  })],
  providers: [ToursService, JwtStrategyService]
})

export class ToursModule {}
