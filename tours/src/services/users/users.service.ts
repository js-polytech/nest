import {Injectable} from '@nestjs/common';
import {User, UserDocument} from '../../schemas/user';
import {InjectModel} from '@nestjs/mongoose';
import {Model} from 'mongoose';
import {JwtModule, JwtService} from '@nestjs/jwt';
import {UserDto} from "../../dto/user-dto";


@Injectable()
export class UsersService {
    constructor (@InjectModel(User.name) private userModel: Model<UserDocument>,
    private jwtService: JwtService) {
        console.log('userService run')
    }


    async getAllUsers(): Promise<User[]> {
        return this.userModel.find();
    }
    // обрабатываем id из объекта param
    async getUserById(id):  Promise<User> {
        return this.userModel.findById(id);
    }

    async sendUser(data):  Promise<User> {
        const userData = new this.userModel(data);
        return userData.save();

    }

    async updateUsers(id: string, body): Promise<User> {
        return this.userModel.findByIdAndUpdate(id, body);
    }

    async deleteUsers():  Promise<User> {
        return this.userModel.remove()
    }

    async deleteUsersById(id: string):  Promise<User> {
        return this.userModel.findByIdAndRemove(id);
    }

    async checkAuthUser (login: string, psw: string): Promise<User[]> {
        const userArr = await this.userModel.find({login: login, psw: psw});
        return userArr.length === 0 ? null : userArr;
    }

    async checkRegUser (login: string): Promise<User[]> {
        return this.userModel.find({login: login});
}


    async login(user: UserDto){
        // формируем уникальный ключ :
        const payload = { login: user.login , psw: user.psw};
        const userFromDb = await this.userModel.find( {login: user.login});
        return {
            id: userFromDb[0]._id,
            access_token: this.jwtService.sign(payload)
        };
    }


//     postUsers(): string {
//         return "service user post data";
//     }
//
//     deleteUsers(): string {
//         return "service all users delete ";
//     }
// //явно указываем свойство id
//     deleteUserById(id: string ): string {
//         return "service delete user with id" + id;
//     }

}
